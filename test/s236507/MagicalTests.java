package s236507;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import social.NoSuchCodeException;
import social.PersonExistsException;
import social.Social;

public class MagicalTests {
	
	@Test(expected = PersonExistsException.class)
	public void test1() throws PersonExistsException, NoSuchCodeException {
		Social social = new Social();
		social.addPerson("TheBoyWhoLived", "Harry", "Potter");
		social.addPerson("OurKing", "Ronald", "Weasley");
		social.addPerson("BrightestOfHerAge", "Hermione", "Granger");
		
		assertEquals("TheBoyWhoLived Harry Potter", social.getPerson("TheBoyWhoLived"));
		assertEquals("OurKing Ronald Weasley", social.getPerson("OurKing"));
		assertEquals("BrightestOfHerAge Hermione Granger", social.getPerson("BrightestOfHerAge"));
		
		social.addPerson("TheBoyWhoLived", "Harvey", "Ponter");
	}
	
	@Test(expected = NoSuchCodeException.class)
	public void test2() throws PersonExistsException, NoSuchCodeException {
		Social social = new Social();
		social.addPerson("TheBoyWhoLived", "Harry", "Potter");
		social.addPerson("OurKing", "Ronald", "Weasley");
		social.addPerson("BrightestOfHerAge", "Hermione", "Granger");
		
		social.addFriendship("TheBoyWhoLived", "OurKing");
		social.addFriendship("TheBoyWhoLived", "BrightestOfHerAge");
		social.addFriendship("BrightestOfHerAge", "OurKing");
		
		assertTrue(social.listOfFriends("TheBoyWhoLived").contains("OurKing"));
		assertTrue(social.listOfFriends("TheBoyWhoLived").contains("BrightestOfHerAge"));
		assertTrue(social.listOfFriends("OurKing").contains("TheBoyWhoLived"));
		assertTrue(social.listOfFriends("OurKing").contains("BrightestOfHerAge"));
		assertTrue(social.listOfFriends("BrightestOfHerAge").contains("TheBoyWhoLived"));
		assertTrue(social.listOfFriends("BrightestOfHerAge").contains("OurKing"));
		
		social.addPerson("SaintLike", "Fred", "Weasley");
		social.addPerson("Unalived", "George", "Weasley");
		social.addPerson("TougherThanYou", "Ginny", "Weasley");
		social.addFriendship("SaintLike", "Unalived");
		social.addFriendship("SaintLike", "TougherThanYou");
		social.addFriendship("SaintLike", "TheBoyWhoLived");
		
		assertTrue(social.friendsOfFriends("TheBoyWhoLived").contains("Unalived"));
		assertTrue(social.friendsOfFriends("TheBoyWhoLived").contains("TougherThanYou"));
		assertFalse(social.friendsOfFriends("TheBoyWhoLived").contains("TheBoyWhoLived"));
		
		social.addFriendship("HeWhoMustNotBeNamed", "AlbusPercivalWulfricBrianDumbledore");
	}
	
	@Test
	public void testGroups() throws PersonExistsException, NoSuchCodeException {
		Social social = new Social();
		social.addPerson("TheBoyWhoLived", "Harry", "Potter");
		social.addPerson("OurKing", "Ronald", "Weasley");
		social.addPerson("BrightestOfHerAge", "Hermione", "Granger");
		social.addPerson("SaintLike", "Fred", "Weasley");
		social.addPerson("Unalived", "George", "Weasley");
		social.addPerson("TougherThanYou", "Ginny", "Weasley");
		social.addPerson("TheCoolBro", "Bill", "Weasley");
		social.addPerson("WorkingWithDragons", "Charlie", "Weasley");
		social.addPerson("Perfect", "Percy", "Weasley");
		social.addPerson("IDontLikeSnakes", "Arthur", "Weasley");
		social.addPerson("MamaWeasley", "Molly", "Weasley");
		social.addPerson("Slimeball", "Severus", "Snape");
		social.addPerson("TheHeadmastah", "Albus", "Dumbledore");
		social.addPerson("Moony", "Remus", "Lupin");
		social.addPerson("Wormtail", "Peter", "Pettigrew");
		social.addPerson("Padfoot", "Sirius", "Black");
		social.addPerson("Prongs", "James", "Potter");
		
		social.addGroup("Ginger Family");
		social.addGroup("Marauders");
		social.addGroup("Six Feet Under");
		social.addGroup("Also animals");
		
		assertTrue(social.listOfGroups().contains("Ginger Family"));
		assertTrue(social.listOfGroups().contains("Marauders"));
		assertTrue(social.listOfGroups().contains("Six Feet Under"));
		
		social.addPersonToGroup("MamaWeasley", "Ginger Family");
		social.addPersonToGroup("IDontLikeSnakes", "Ginger Family");
		social.addPersonToGroup("TheCoolBro", "Ginger Family");
		social.addPersonToGroup("WorkingWithDragons", "Ginger Family");
		social.addPersonToGroup("Perfect", "Ginger Family");
		social.addPersonToGroup("SaintLike", "Ginger Family");
		social.addPersonToGroup("Unalived", "Ginger Family");
		social.addPersonToGroup("OurKing", "Ginger Family");
		social.addPersonToGroup("TougherThanYou", "Ginger Family");
		
		social.addPersonToGroup("Moony", "Marauders");
		social.addPersonToGroup("Wormtail", "Marauders");
		social.addPersonToGroup("Padfoot", "Marauders");
		social.addPersonToGroup("Prongs", "Marauders");
		
		social.addPersonToGroup("Slimeball", "Six Feet Under");
		social.addPersonToGroup("TheHeadmastah", "Six Feet Under");
		social.addPersonToGroup("Moony", "Six Feet Under");
		social.addPersonToGroup("Wormtail", "Six Feet Under");
		social.addPersonToGroup("Padfoot", "Six Feet Under");
		social.addPersonToGroup("Prongs", "Six Feet Under");
		social.addPersonToGroup("Unalived", "Six Feet Under");
		
		social.addPersonToGroup("Padfoot", "Also animals");
		
		assertTrue(social.listOfPeopleInGroup("Ginger Family").contains("MamaWeasley"));
		assertTrue(social.listOfPeopleInGroup("Ginger Family").contains("IDontLikeSnakes"));
		assertTrue(social.listOfPeopleInGroup("Ginger Family").contains("TheCoolBro"));
		assertTrue(social.listOfPeopleInGroup("Ginger Family").contains("WorkingWithDragons"));
		assertTrue(social.listOfPeopleInGroup("Ginger Family").contains("Perfect"));
		assertTrue(social.listOfPeopleInGroup("Ginger Family").contains("SaintLike"));
		assertTrue(social.listOfPeopleInGroup("Ginger Family").contains("OurKing"));
		assertTrue(social.listOfPeopleInGroup("Ginger Family").contains("Unalived"));
		assertTrue(social.listOfPeopleInGroup("Ginger Family").contains("TougherThanYou"));
		
		assertTrue(social.listOfPeopleInGroup("Marauders").contains("Moony"));
		assertTrue(social.listOfPeopleInGroup("Marauders").contains("Wormtail"));
		assertTrue(social.listOfPeopleInGroup("Marauders").contains("Padfoot"));
		assertTrue(social.listOfPeopleInGroup("Marauders").contains("Prongs"));
		
		assertNull(social.listOfPeopleInGroup("Dumbledore's Army"));
		
		social.addFriendship("TheBoyWhoLived", "OurKing");
		social.addFriendship("TheBoyWhoLived", "BrightestOfHerAge");
		social.addFriendship("BrightestOfHerAge", "OurKing");
		social.addFriendship("TheBoyWhoLived", "TougherThanYou");
		social.addFriendship("TheBoyWhoLived", "TheHeadmastah");
		social.addFriendship("TheBoyWhoLived", "Prongs");
		
		assertEquals("TheBoyWhoLived", social.personWithLargestNumberOfFriends());
		assertEquals("BrightestOfHerAge", social.personWithMostFriendsOfFriends());
		assertEquals("Ginger Family", social.largestGroup());
		assertEquals("Padfoot", social.personInLargestNumberOfGroups());
	}
	
}
